import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;
import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.*;

public class DoubleStack {

   public static void main (String[] argum) throws CloneNotSupportedException {

       /*DoubleStack ds = new DoubleStack();
       DoubleStack dsa = new DoubleStack();
       //LinkedList dsa = new LinkedList<Double>(Arrays.asList(1.0, 2.0));
       //dsa.stack.removeLast();
       dsa.push(2.3);
       System.out.println(ds);
       System.out.println(dsa);
       System.out.println("equals: " + ds.equals(dsa));
       System.out.println("stEmpty: " + ds.stEmpty());
       ds.push(2.3);
       ds.push(2.3);
       ds.push(2.3);
       ds.push(2.3);
       ds.push(2.3);
       ds.push(2.3);
       ds.push(2.3);
       ds.push(2.3);
       ds.push(2.3);
       ds.push(2.3);
       ds.push(2.3);
       ds.push(2.3);
       System.out.println(ds.toString());
       ds.op("+");
       System.out.println(ds.toString());
       ds.op("-");
       System.out.println(ds.toString());
       ds.op("*");
       System.out.println(ds.toString());
       ds.op("/");
       System.out.println(ds.toString());
       System.out.println(ds.tos());
       System.out.println(ds.toString());
       System.out.println("ds : " + dsa.toString());
       DoubleStack kloon = (DoubleStack) ds.clone();
       System.out.println("kloon : " + kloon.toString());
       System.out.println("dsa : " + ds.toString());
       System.out.println("equals: " + ds.equals(kloon));
       System.out.println("equals: " + kloon.equals(ds));
       System.out.println(DoubleStack.interpret("35. 10. -3. 13. * +  "));

       //boolean a = ds==dsa;
       //System.out.println(a);

       // TODO!!! Your tests here!
       */
   }

    private LinkedList<Double> stack;

    DoubleStack() {
        //stack = new LinkedList<Double>(Arrays.asList(1.0, 2.0));
        stack = new LinkedList<Double>();
        //System.out.println(stack);
        //System.out.println(stack.size());

   }

   @Override
   public Object clone() throws CloneNotSupportedException {
       DoubleStack clone = new DoubleStack();
       for ( double d : this.stack ) {
           clone.push(d);
       }

       return clone;
   }

   public boolean stEmpty() {
      return Integer.valueOf(stack.size()).equals(0);
   }

   public void push (double a) {
       stack.add(a);
   }


   public double pop() {
       if (stEmpty()) throw new NoSuchElementException("The stack is empty");

       double top = stack.getLast();
       stack.removeLast();
       return top;
   }

   public void op (String s) {
       if (stEmpty()) throw new NoSuchElementException("The stack is empty");
       double a = pop();
       if (stEmpty()) {
           push(a);
           throw new NoSuchElementException("The stack is empty");
       }
       double b = pop();
       if (s.equals("+")) push(b+a);
       else if (s.equals("-")) push(b-a);
       else if (s.equals("*")) push(b*a);
       else if (s.equals("/")) push(b/a);
       else {
           throw new NumberFormatException("User input: " + s + "<-- operator not valid");
       }

   }
  
   public double tos() {
       if (stEmpty()) throw new NoSuchElementException("The stack is empty");
       return stack.getLast();
   }

   @Override
   public boolean equals (Object o) {
        try {
            DoubleStack oStack = (DoubleStack) o;
        }catch (ClassCastException e) {
            return false;
        }
       DoubleStack oStack = (DoubleStack) o;
       if (!Integer.valueOf(stack.size()).equals(oStack.stack.size())) return false;

       for (int i = 0; i < stack.size(); i++) {
           if (!stack.get(i).equals(oStack.stack.get(i))) return false;
           //System.out.println(i);
       }
       return true;
   }

   @Override
   public String toString() {
       //String s = "";
       //for (Double d: stack) {
       //    s += String.valueOf(d) + " ";
       //}
       return stack.toString();
   }

   public static double interpret (String pol) {
       DoubleStack polish = new DoubleStack();
       StringTokenizer tok = new StringTokenizer(pol);
       while (tok.hasMoreTokens()){
           String s = tok.nextToken();
           //System.out.println(s);
           try {
               polish.push(Double.parseDouble(s));
           }catch (NumberFormatException e){
               if (s.equals("+")) {
                   polish.op("+");

               } else if (s.equals("-")) {
                   polish.op("-");

               } else if (s.equals("*")) {
                   polish.op("*");

               } else if (s.equals("/")) {
                   polish.op("/");

               } else {
                   throw new RuntimeException("Invalid input -->" + s + "Doubles and operators only!");

               }
           }
           //System.out.println(polish.toString());
       }
       if (polish.stack.size()>1) throw new RuntimeException("Too many elements for operators");
       return polish.tos();
   }

}

